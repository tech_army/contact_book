import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

def create_contact():
    name = name_entry.get()
    phone = phone_entry.get()
    if not phone.isdigit() or len(phone) != 10:
        messagebox.showwarning("Invalid Phone Number", "Please enter a valid ten-digit phone number.")
        return
    contacts.append((name, phone))
    update_contacts()
    # Clear the entry fields
    name_entry.delete(0, tk.END)
    phone_entry.delete(0, tk.END)

def update_contacts():
    contact_table.delete(*contact_table.get_children())
    for contact in contacts:
        contact_table.insert('', tk.END, values=contact)

def delete_contact():
    selected_item = contact_table.selection()
    if not selected_item:
        messagebox.showwarning("No Selection", "Please select a contact to delete.")
        return
    # Get the selected contact's details
    name, phone = contact_table.item(selected_item)['values']
    print("Deleting contact:", name, phone)  # Debug print
    # Attempt to remove the contact from the list
    try:
        contacts.remove((name, phone))
        update_contacts()
    except ValueError:
        messagebox.showerror("Contact Not Found", "The selected contact was not found.")

def edit_contact():
    selected_item = contact_table.selection()
    if not selected_item:
        messagebox.showwarning("No Selection", "Please select a contact to edit.")
        return
    # Get the selected contact's details
    old_name, old_phone = contact_table.item(selected_item)['values']
    new_name = name_entry.get()
    new_phone = phone_entry.get()
    if not new_phone.isdigit() or len(new_phone) != 10:
        messagebox.showwarning("Invalid Phone Number", "Please enter a valid ten-digit phone number.")
        return
    # Update the contact details
    index = contacts.index((old_name, old_phone))
    contacts[index] = (new_name, new_phone)
    update_contacts()
    # Clear the entry fields
    name_entry.delete(0, tk.END)
    phone_entry.delete(0, tk.END)

def on_select(event):
    selected_item = contact_table.selection()
    if selected_item:
        name, phone = contact_table.item(selected_item)['values']
        name_entry.delete(0, tk.END)
        name_entry.insert(0, name)
        phone_entry.delete(0, tk.END)
        phone_entry.insert(0, phone)

contacts = []

root = tk.Tk()
root.title("Contact Book")
root.geometry("600x400")

# Add colors to the background
root.configure(bg="#f0f0f0")

# Create frames
top_frame = tk.Frame(root, bg="#c0c0c0", pady=10)
top_frame.pack(fill=tk.X)

middle_frame = tk.Frame(root, bg="#f0f0f0")
middle_frame.pack(expand=True, fill=tk.BOTH)

bottom_frame = tk.Frame(root, bg="#c0c0c0", pady=10)
bottom_frame.pack(fill=tk.X)

# Search Entry and Button
search_label = tk.Label(top_frame, text="Search:", bg="#c0c0c0", font=("Arial", 12))
search_label.pack(side=tk.LEFT, padx=10)
search_entry = tk.Entry(top_frame, font=("Arial", 12))
search_entry.pack(side=tk.LEFT, padx=10)
search_button = tk.Button(top_frame, text="Search", command=create_contact, font=("Arial", 12), bg="#4CAF50", fg="white")
search_button.pack(side=tk.LEFT, padx=10)

# Contact List Table
contact_table = ttk.Treeview(middle_frame, columns=("Name", "Phone"), show="headings")
contact_table.heading("Name", text="Name", anchor="center")
contact_table.heading("Phone", text="Phone", anchor="center")
contact_table.column("Name", width=300, anchor="center")
contact_table.column("Phone", width=300, anchor="center")

# Set font style for headings
style = ttk.Style()
style.configure("Treeview.Heading", font=("Arial", 16, "bold"), foreground="blue")

contact_table.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)

# Bind Treeview select event
contact_table.bind("<<TreeviewSelect>>", on_select)

# Create New Contact
name_label = tk.Label(bottom_frame, text="Name:", bg="#c0c0c0", font=("Arial", 12))
name_label.grid(row=0, column=0, padx=(10, 0), pady=10, sticky="e")
name_entry = tk.Entry(bottom_frame, font=("Arial", 12))
name_entry.grid(row=0, column=1, padx=10, pady=10)
phone_label = tk.Label(bottom_frame, text="Phone:", bg="#c0c0c0", font=("Arial", 12))
phone_label.grid(row=1, column=0, padx=(10, 0), pady=10, sticky="e")

# Phone number validation function
def validate_phone(entry):
    if entry.isdigit() and len(entry) <= 10:
        return True
    elif not entry:
        return True
    else:
        return False

validate_phone_cmd = root.register(validate_phone)
phone_entry = tk.Entry(bottom_frame, font=("Arial", 12), validate="key", validatecommand=(validate_phone_cmd, '%P'))
phone_entry.grid(row=1, column=1, padx=10, pady=10)

create_button = tk.Button(bottom_frame, text="Create Contact", command=create_contact, font=("Arial", 12), bg="#4CAF50", fg="white")
create_button.grid(row=2, column=1, padx=10, pady=(0, 10))

edit_button = tk.Button(bottom_frame, text="Edit Contact", command=edit_contact, font=("Arial", 12), bg="#FFC107", fg="black")
edit_button.grid(row=3, column=0, padx=10, pady=10)

delete_button = tk.Button(bottom_frame, text="Delete Contact", command=delete_contact, font=("Arial", 12), bg="#F44336", fg="white")
delete_button.grid(row=3, column=1, padx=10, pady=10)

root.mainloop()
