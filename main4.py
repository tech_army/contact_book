from tkinter import *

def validate_signup():
    # Replace the following with your own validation logic
    username = username_entry.get()
    password = password_entry.get()
    confirm_password = confirm_password_entry.get()

    if username == "" or password == "" or confirm_password == "":
        success_label.config(text="Please fill in all fields!", fg="red")
        return

    if password != confirm_password:
        success_label.config(text="Passwords do not match!", fg="red")
        return

    # Show a success message or redirect to another page
    success_label.config(text="Signup successful!", fg="green")

root = Tk()
root.title("Sign Up")
root.geometry("400x400")
root.configure(background="#F0F2F5")

# Create a frame for the logo and title
logo_frame = Frame(root, bg="#F0F2F5")
logo_frame.pack(pady=20)

logo_label = Label(logo_frame, text="CONTACT BOOK", font=("Arial", 32, "bold"), bg="#F0F2F5", fg="#414257")
logo_label.pack()

title_label = Label(logo_frame, text="Where memories and contacts meet !!!", font=("Arial", 18), bg="#F0F2F5", fg="#414257")
title_label.pack()

# Create a frame for the signup form
signup_frame = Frame(root, bg="#F0F2F5")
signup_frame.pack(pady=20)

username_label = Label(signup_frame, text="Username:", font=("Arial", 14), bg="#F0F2F5", fg="#414257")
username_label.grid(row=0, column=0, sticky=W, padx=5, pady=5)

username_entry = Entry(signup_frame, font=("Arial", 14), bg="white")
username_entry.grid(row=0, column=1, sticky=W, padx=5, pady=5)

password_label = Label(signup_frame, text="Password:", font=("Arial", 14), bg="#F0F2F5", fg="#414257")
password_label.grid(row=1, column=0, sticky=W, padx=5, pady=5)

password_entry = Entry(signup_frame, font=("Arial", 14), show="*", bg="white")
password_entry.grid(row=1, column=1, sticky=W, padx=5, pady=5)

confirm_password_label = Label(signup_frame, text="Confirm Password:", font=("Arial", 14), bg="#F0F2F5", fg="#414257")
confirm_password_label.grid(row=2, column=0, sticky=W, padx=5, pady=5)

confirm_password_entry = Entry(signup_frame, font=("Arial", 14), show="*", bg="white")
confirm_password_entry.grid(row=2, column=1, sticky=W, padx=5, pady=5)

signup_button = Button(signup_frame, text="Sign Up", font=("Arial", 14), bg="#414257", fg="white", command=validate_signup)
signup_button.grid(row=3, columnspan=2, pady=10)

success_label = Label(signup_frame, text="", font=("Arial", 10), bg="#F0F2F5", fg="green")
success_label.grid(row=4, columnspan=2)

root.mainloop()