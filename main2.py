import tkinter as tk
from tkinter import messagebox

def open_update_profile_window():
    update_profile_window = tk.Tk()
    update_profile_window.title("Update Profile")

    # Set background color to light blue
    update_profile_window.configure(bg="#ADD8E6")

    # Create a label for the heading
    heading_label = tk.Label(update_profile_window, text="Update Profile", font=('Helvetica', 16, 'bold'), bg="#ADD8E6")
    heading_label.pack(pady=10)

    # Create labels and entry fields for age, gender, volunteer or disabled, phone number, and address
    age_label = tk.Label(update_profile_window, text="Age:")
    age_label.pack(pady=5)
    age_entry = tk.Entry(update_profile_window)
    age_entry.pack(pady=5)

    gender_label = tk.Label(update_profile_window, text="Gender:")
    gender_label.pack()
    gender_var = tk.StringVar()
    gender_var.set("Male")  # Default value
    male_radio = tk.Radiobutton(update_profile_window, text="Male", variable=gender_var, value="Male")
    female_radio = tk.Radiobutton(update_profile_window, text="Female", variable=gender_var, value="Female")
    others_radio = tk.Radiobutton(update_profile_window, text="Others", variable=gender_var, value="Others")

    male_radio.pack()
    female_radio.pack()
    others_radio.pack()

    user_type_label = tk.Label(update_profile_window, text="User Type:")
    user_type_label.pack()
    user_type_var = tk.StringVar()
    user_type_var.set("Volunteer")  # Default user type
    user_type_menu = tk.OptionMenu(update_profile_window, user_type_var, "Volunteer", "Disabled")
    user_type_menu.pack()

    phone_label = tk.Label(update_profile_window, text="Phone No:")
    phone_label.pack(pady=5)
    phone_entry = tk.Entry(update_profile_window)
    phone_entry.pack(pady=5)

    address_label = tk.Label(update_profile_window, text="Address:")
    address_label.pack(pady=5)
    address_entry = tk.Entry(update_profile_window)
    address_entry.pack(pady=5)

    # Create a button to close the update profile window
    close_button = tk.Button(update_profile_window, text="Close", command=update_profile_window.destroy)
    close_button.pack(pady=10)

    # Create a button to continue (additional functionality can be added)
    continue_button = tk.Button(update_profile_window, text="Continue", command=lambda: continue_update_profile(
        age_entry.get(), gender_var.get(), user_type_var.get(), phone_entry.get(), address_entry.get()))
    continue_button.pack(pady=10)

    update_profile_window.mainloop()

def continue_update_profile(age, gender, user_type, phone_no, address):
    # Add functionality for the "Continue" button here...
    if not phone_no.isdigit() or len(phone_no) != 10:
        messagebox.showerror("Error", "Enter a 10-digit phone number")
        return

    if user_type == "Volunteer" and (not age.isdigit() or int(age) < 18):
        messagebox.showerror("Error", "Not eligible as a volunteer (Age should be 18 or greater)")
        return

    success_message = f"Profile successfully updated!\n\n" \
                      f"Age: {age}\n" \
                      f"Gender: {gender}\n" \
                      f"User Type: {user_type}\n" \
                      f"Phone No: {phone_no}\n" \
                      f"Address: {address}"
    messagebox.showinfo("Success", success_message)

# Create the main window
main_window = tk.Tk()
main_window.title("Welcome")

# Set background color to light blue
main_window.configure(bg="#ADD8E6")

# Create a label for the heading
heading_label = tk.Label(main_window, text="Welcome", font=('Helvetica', 16, 'bold'), bg="#ADD8E6")
heading_label.pack(pady=10)

# Create a button to open the update profile window
update_profile_button = tk.Button(main_window, text="Update Profile", command=open_update_profile_window)
update_profile_button.pack(pady=10)

# Create a button to continue
continue_button = tk.Button(main_window, text="Continue", command=continue_update_profile)
continue_button.pack(pady=10)

# Calculate the screen width and height
screen_width = main_window.winfo_screenwidth()
screen_height = main_window.winfo_screenheight()

# Set the window size
window_width = 300
window_height = 250

# Calculate the window position to center it on the screen
x_position = (screen_width - window_width) // 2
y_position = (screen_height - window_height) // 2

# Set the window size and position
main_window.geometry(f"{window_width}x{window_height}+{x_position}+{y_position}")

# Start the GUI main loop
main_window.mainloop()