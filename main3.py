import tkinter as tk
from PIL import Image, ImageTk

def login():
    username = username_entry.get()
    password = password_entry.get()

    # Check if username and password fields are empty
    if not username or not password:
        # Prompt user to sign up
        signup_prompt_label.config(text="Please sign up", fg="red")
        return
    
    # Placeholder for actual login logic
    print("Login button clicked")
    # Here you can add your logic to verify the credentials
    
def signup(event=None):
    # Placeholder function for signup action
    print("Sign Up link clicked")

# Create main window
root = tk.Tk()
root.title("Contact Book")

# Load background image
background_image = Image.open("C:\\Users\\Dell\\Desktop\\projects\\venv\\Scripts\\Login background.jpeg")
background_photo = ImageTk.PhotoImage(background_image)

# Create a canvas to place the background image
canvas = tk.Canvas(root, width=background_image.width, height=background_image.height)
canvas.pack(fill="both", expand=True)

# Set background image
canvas.create_image(0, 0, image=background_photo, anchor="nw")

# Create heading
heading = tk.Label(root, text="CONTACT BOOK", font=("Helvetica", 16), bg="#4682B4", fg="white")
heading.place(relx=0.1, rely=0.2, anchor="w")

# Create username label and entry
username_label = tk.Label(root, text="Username:", bg="#F0F0F0", font=("Helvetica", 12))
username_label.place(relx=0.1, rely=0.3, anchor="w")
username_entry = tk.Entry(root, bg="lightgray", font=("Helvetica", 12))
username_entry.place(relx=0.1, rely=0.35, anchor="w")

# Create password label and entry
password_label = tk.Label(root, text="Password:", bg="#F0F0F0", font=("Helvetica", 12))
password_label.place(relx=0.1, rely=0.4, anchor="w")
password_entry = tk.Entry(root, show="*", bg="lightgray", font=("Helvetica", 12))
password_entry.place(relx=0.1, rely=0.45, anchor="w")

# Create login button
login_button = tk.Button(root, text="Login", command=login, bg="#4682B4", fg="white", font=("Helvetica", 12, "bold"), padx=10, pady=5)  # Increased font size and padding
login_button.place(relx=0.1, rely=0.52, anchor="w")

# Create label for signup prompt (acting as a hyperlink)
signup_prompt_label = tk.Label(root, text="Dont have an account?", bg="#F0F0F0", font=("Helvetica", 10, "italic"))
signup_prompt_label.place(relx=0.1, rely=0.59, anchor="w")

signup_link = tk.Label(root, text="Sign up", bg="#F0F0F0", fg="blue", font=("Helvetica", 10, "underline"), cursor="hand2")
signup_link.place(relx=0.23, rely=0.59, anchor="w")
signup_link.bind("<Button-1>", lambda event: signup(event))

root.mainloop()
