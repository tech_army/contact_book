import tkinter as tk
from tkinter import ttk, messagebox
from plyer import notification
import threading
import time
import sqlite3

class ContactBook:
    def __init__(self, master):
        self.master = master
        self.master.title("Login")
        self.master.geometry("500x300")

        self.conn = sqlite3.connect('contacts.db')
        self.cursor = self.conn.cursor()

        self.cursor.execute('''CREATE TABLE IF NOT EXISTS contacts
                             (id INTEGER PRIMARY KEY, name TEXT, phone TEXT)''')

        self.cursor.execute('''CREATE TABLE IF NOT EXISTS users
                             (id INTEGER PRIMARY KEY, username TEXT, password TEXT)''')

        self.login_frame = tk.Frame(master, bg="light blue")
        self.login_frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        
        self.heading_label = tk.Label(self.login_frame, text="CONTACT BOOK", font=("Helvetica", 20, "bold"), bg="light blue")
        self.heading_label.grid(row=0, column=0, columnspan=50, pady=10)
        
        self.username_label = tk.Label(self.login_frame, text="Username:")
        self.username_label.grid(row=0, column=0, padx=8, pady=5)
        self.username_entry = tk.Entry(self.login_frame)
        self.username_entry.grid(row=0, column=1, padx=8, pady=5)

        self.password_label = tk.Label(self.login_frame, text="Password:")
        self.password_label.grid(row=1, column=0, padx=8, pady=5)
        self.password_entry = tk.Entry(self.login_frame, show="*")
        self.password_entry.grid(row=1, column=1, padx=8, pady=5)

        self.login_button = tk.Button(self.login_frame, text="Login", command=self.validate_login)
        self.login_button.grid(row=2, column=0, columnspan=2, pady=10)

        self.signup_button = tk.Button(self.login_frame, text="Sign Up", command=self.sign_up)
        self.signup_button.grid(row=3, column=0, columnspan=2, pady=10)

        self.contacts_frame = tk.Frame(master, bg="light green")
        self.contacts_frame.pack(expand=True, fill="both")

        self.top_frame = tk.Frame(self.contacts_frame, bg="light yellow")
        self.top_frame.pack(fill=tk.X)

        self.middle_frame = tk.Frame(self.contacts_frame, bg="light yellow")
        self.middle_frame.pack(expand=True, fill=tk.BOTH)

        self.bottom_frame = tk.Frame(self.contacts_frame, bg="light yellow")
        self.bottom_frame.pack(fill=tk.X)

        self.search_label = tk.Label(self.top_frame, text="Search:")
        self.search_label.pack(side=tk.LEFT, padx=10)
        self.search_entry = tk.Entry(self.top_frame)
        self.search_entry.pack(side=tk.LEFT, padx=10)
        self.search_button = tk.Button(self.top_frame, text="Search", command=self.search_contacts)
        self.search_button.pack(side=tk.LEFT, padx=10)

        self.contact_table = ttk.Treeview(self.middle_frame, columns=("Name", "Phone"), show="headings")
        self.contact_table.heading("Name", text="Name", anchor="center")
        self.contact_table.heading("Phone", text="Phone", anchor="center")
        self.contact_table.column("Name", width=300, anchor="center")
        self.contact_table.column("Phone", width=300, anchor="center")
        self.contact_table.pack(expand=True, fill=tk.BOTH, padx=10, pady=10)
        self.contact_table.bind("<Double-1>", self.set_remainder_for_contact)

        self.name_label = tk.Label(self.bottom_frame, text="Name:")
        self.name_label.grid(row=0, column=0, padx=(5, 0), pady=5, sticky="e")
        self.name_entry = tk.Entry(self.bottom_frame)
        self.name_entry.grid(row=0, column=1, padx=5, pady=5)
        self.phone_label = tk.Label(self.bottom_frame, text="Phone:")
        self.phone_label.grid(row=1, column=0, padx=(5, 0), pady=5, sticky="e")
        self.phone_entry = tk.Entry(self.bottom_frame, validate="key")
        self.phone_entry.grid(row=1, column=1, padx=5, pady=5)
        self.create_button = tk.Button(self.bottom_frame, text="Create Contact", command=self.create_contact)
        self.create_button.grid(row=2, column=1,padx=10,pady=10)
        self.edit_button = tk.Button(self.bottom_frame, text="Edit", command=self.edit_contact)
        self.edit_button.grid(row=3, column=0, padx=(5, 0), pady=(5, 10))
        self.delete_button = tk.Button(self.bottom_frame, text="Delete", command=self.delete_contact)
        self.delete_button.grid(row=3, column=1, padx=(0, 5), pady=(5, 10))
        self.remainder_button = tk.Button(self.bottom_frame, text="Remainder", command=self.open_remainder_page)
        self.remainder_button.grid(row=4, column=0, columnspan=2, pady=10)

        self.update_contacts()
        self.contacts_frame.pack_forget()

    def validate_login(self):
        username = self.username_entry.get()
        password = self.password_entry.get()
        self.cursor.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password))
        user = self.cursor.fetchone()
        if user:
            self.switch_to_contacts_page()
        else:
            messagebox.showerror("Login Failed", "Invalid username or password!")

    def switch_to_contacts_page(self):
        self.master.title("Contact Book")
        self.login_frame.pack_forget()
        self.contacts_frame.pack()

    def sign_up(self):
        username = self.username_entry.get()
        password = self.password_entry.get()
        self.cursor.execute("SELECT * FROM users WHERE username=?", (username,))
        existing_user = self.cursor.fetchone()
        if existing_user:
            messagebox.showerror("Sign Up Failed", "Username already exists!")
        else:
            self.cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password))
            self.conn.commit()
            messagebox.showinfo("Sign Up Successful", "Account created successfully!")
            self.switch_to_contacts_page()

    def create_contact(self):
        name = self.name_entry.get()
        phone = self.phone_entry.get()
        if not phone.isdigit() or len(phone) != 10:
            messagebox.showwarning("Invalid Phone Number", "Please enter a valid ten-digit phone number.")
            return
        self.cursor.execute("INSERT INTO contacts (name, phone) VALUES (?, ?)", (name, phone))
        self.conn.commit()
        self.update_contacts()
        self.name_entry.delete(0, tk.END)
        self.phone_entry.delete(0, tk.END)

    def update_contacts(self):
        self.contact_table.delete(*self.contact_table.get_children())
        self.cursor.execute("SELECT * FROM contacts")
        rows = self.cursor.fetchall()
        for contact in rows:
            self.contact_table.insert('', tk.END, values=(contact[1], contact[2]))

    def edit_contact(self):
        selected_item = self.contact_table.selection()
        if selected_item:
            contact = self.contact_table.item(selected_item, 'values')
            edit_window = tk.Toplevel(self.master)
            edit_window.title("Edit Contact")
            edit_window.geometry("500x350")
            edit_name_label = tk.Label(edit_window, text="Name:")
            edit_name_label.grid(row=0, column=0, padx=10, pady=5)
            edit_name_entry = tk.Entry(edit_window)
            edit_name_entry.grid(row=0, column=1, padx=10, pady=5)
            edit_name_entry.insert(0, contact[0])
            edit_phone_label = tk.Label(edit_window, text="Phone:")
            edit_phone_label.grid(row=1, column=0, padx=10, pady=5)
            edit_phone_entry = tk.Entry(edit_window)
            edit_phone_entry.grid(row=1, column=1, padx=10, pady=5)
            edit_phone_entry.insert(0, contact[1])
            def update_contact():
                new_name = edit_name_entry.get()
                new_phone = edit_phone_entry.get()
                if not new_phone.isdigit() or len(new_phone) != 10:
                    messagebox.showwarning("Invalid Phone Number", "Please enter a valid ten-digit phone number.")
                    return
                self.cursor.execute("UPDATE contacts SET name=?, phone=? WHERE name=?", (new_name, new_phone, contact[0]))
                self.conn.commit()
                self.update_contacts()
                edit_window.destroy()
            update_button = tk.Button(edit_window, text="Update", command=update_contact)
            update_button.grid(row=4, column=0, columnspan=4, padx=10, pady=5)

    def delete_contact(self):
        selected_item = self.contact_table.selection()
        if selected_item:
            contact = self.contact_table.item(selected_item, 'values')
            self.cursor.execute("DELETE FROM contacts WHERE name=?", (contact[0],))
            self.conn.commit()
            self.update_contacts()

    def search_contacts(self):
        search_text = self.search_entry.get().lower()
        if search_text:
            self.cursor.execute("SELECT * FROM contacts WHERE lower(name) LIKE ? OR phone LIKE ?", ('%' + search_text + '%', '%' + search_text + '%'))
            found_contacts = self.cursor.fetchall()
            if found_contacts:
                self.contact_table.delete(*self.contact_table.get_children())
                for contact in found_contacts:
                    self.contact_table.insert('', tk.END, values=(contact[1], contact[2]))
            else:
                messagebox.showinfo("No Matches", "No contacts found matching the search criteria.")
        else:
            self.update_contacts()

    def set_remainder_for_contact(self, event):
        selected_item = self.contact_table.selection()
        if selected_item:
            contact = self.contact_table.item(selected_item, 'values')
            reminder_window = tk.Toplevel(self.master)
            reminder_window.title("Set Reminder")
            reminder_window.geometry("300x150")
            reminder_label = tk.Label(reminder_window, text="Set a reminder for this contact:")
            reminder_label.pack()
            reminder_entry = tk.Entry(reminder_window)
            reminder_entry.pack()
            time_label = tk.Label(reminder_window, text="Time (HH:MM):")
            time_label.pack()
            time_entry = tk.Entry(reminder_window)
            time_entry.pack()
            date_label = tk.Label(reminder_window, text="Date (DD/MM/YYYY):")
            date_label.pack()
            date_entry = tk.Entry(reminder_window)
            date_entry.pack()
            submit_button = tk.Button(reminder_window, text="Submit", command=lambda: self.submit_reminder(reminder_window, contact[0], reminder_entry.get(), time_entry.get(), date_entry.get()))
            submit_button.pack()

    def submit_reminder(self, reminder_window, contact_name, reminder, time_str, date_str):
        reminder_window.destroy()
        try:
            assigned_time = time.strptime(time_str, "%H:%M")
        except ValueError:
            messagebox.showerror("Error", "Invalid time format. Please use HH:MM.")
            return
        try:
            assigned_date = time.strptime(date_str, "%d/%m/%Y")
        except ValueError:
            messagebox.showerror("Error", "Invalid date format. Please use DD/MM/YYYY.")
            return
        scheduled_time = time.mktime((assigned_date.tm_year, assigned_date.tm_mon, assigned_date.tm_mday, assigned_time.tm_hour, assigned_time.tm_min, 0, 0, 0, -1))
        current_time = time.time()
        time_difference = scheduled_time - current_time
        if time_difference > 0:
            time.sleep(time_difference)
            notification.notify(
                title="Reminder Notification",
                message=f"Reminder: {reminder}",
                app_name="Reminder Manager"
            )
            self.cursor.execute("INSERT INTO remainders (contact_name, reminder, time, date) VALUES (?, ?, ?, ?)", (contact_name, reminder, time_str, date_str))
            self.conn.commit()
        else:
            messagebox.showwarning("Invalid Reminder Time", "The reminder time has already passed.")

    def open_remainder_page(self):
        remainder_window = tk.Toplevel(self.master)
        remainder_window.title("Reminder Page")
        remainder_window.geometry("600x400")
        app = ReminderApp(remainder_window)

class ReminderApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Contact Reminder Manager")
        self.master.geometry("600x400")

        self.remainders = []

        self.remainders_label = tk.Label(master, text="Reminders")
        self.remainders_label.pack()

        self.remainders_listbox = tk.Listbox(master, selectmode=tk.SINGLE)
        self.remainders_listbox.pack()

        self.exit_button = tk.Button(master, text="Exit", command=self.master.destroy)
        self.exit_button.pack()

        self.conn = sqlite3.connect('remainders.db')
        self.create_table()
        
        self.load_remainders()
        
    def create_table(self):
        self.cursor = self.conn.cursor()
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS remainders (
                id INTEGER PRIMARY KEY,
                contact_name TEXT,
                reminder TEXT,
                time TEXT,
                date TEXT
            )
        ''')
        self.conn.commit()

    def load_remainders(self):
        self.cursor.execute("SELECT * FROM remainders")
        rows = self.cursor.fetchall()

        for row in rows:
            self.remainders.append({"contact_name": row[1], "reminder": row[2], "time": row[3], "date": row[4]})
            self.remainders_listbox.insert(tk.END, row[2])

    def schedule_notification(self, reminder, time_str):
        current_time = time.localtime()

        assigned_time = time.strptime(time_str, "%H:%M")
        assigned_time_seconds = assigned_time.tm_hour * 3600 + assigned_time.tm_min * 60
        current_time_seconds = current_time.tm_hour * 3600 + current_time.tm_min * 60
        time_difference = assigned_time_seconds - current_time_seconds

        if time_difference > 0:
            time.sleep(time_difference)

            notification.notify(
                title="Reminder Notification",
                message=f"Reminder: {reminder}",
                app_name="Reminder Manager"
            )

if __name__ == "__main__":
    root = tk.Tk()
    root.title("Contact Reminder Manager")
    root.geometry("900x700")

    app = ContactBook(root)
    root.mainloop()